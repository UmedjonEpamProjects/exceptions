package com.epam.codingbat;

/*
* Раздел: Warmup 1
* Задача: inMax
* Условие: Given three int values, a b c, return the largest.
* */
public class Max {
    public int intMax(int a, int b, int c) {
        if (a > b && a>c){
            return a;

        }else if (b > a && b>c) {
            return b;

        }
        else{
            return c;
        }
    }

    public static void main(String[] args) {

    }
}
