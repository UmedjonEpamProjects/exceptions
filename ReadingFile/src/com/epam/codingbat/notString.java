package com.epam.codingbat;
/*
* Раздел: Warmup 1
* Задача: notString
* Условие: Given a string, return a new string where "not " has been added to the front.
*          However, if the string already begins with "not", return the string unchanged.
*          Note: use .equals() to compare 2 strings.
* */

public class notString {
    public String notString(String str) {
        String front =  "not";

        if (str == front) {
            return str;
        } else if (str.length() < 4) {
            return front + " " + str;
        } else if (str.substring(0, 3).equals(front)) {
            return str;
        }
        else{
            return front +" "+ str;
        }
    }

    public static void main(String[] args) {

    }
}
